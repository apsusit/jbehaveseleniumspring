package br.com.apsusit.jbehave.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by wesley on 5/29/16.
 */
public abstract class AbstractPage {

    private static WebDriver driver;

    public AbstractPage() {
        System.setProperty("webdriver.chrome.driver", "/home/wesley/Projects/lib/chromedriver");
        if (driver == null) {
            driver = new ChromeDriver();
        }
    }

    public static WebDriver getDriver() {
        return driver;
    }
}
