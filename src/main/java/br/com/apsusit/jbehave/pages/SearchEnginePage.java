package br.com.apsusit.jbehave.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.stereotype.Component;

/**
 * Created by wesley on 5/28/16.
 */

@Component
public class SearchEnginePage extends AbstractPage {

    @FindBy(id = "lst-ib")
    private WebElement input;

    public SearchEnginePage() {
        PageFactory.initElements(getDriver(), this);
    }

    public void searchForText(String searchCriteria) {
        this.input.sendKeys(searchCriteria);
        this.input.sendKeys(Keys.ENTER);
    }

    public WebElement getInput() {
        return input;
    }
}
