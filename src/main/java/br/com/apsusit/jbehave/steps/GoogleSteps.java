package br.com.apsusit.jbehave.steps;

import br.com.apsusit.jbehave.pages.SearchEnginePage;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wesley on 5/29/16.
 */

@Component
public class GoogleSteps {

    @Autowired
    private SearchEnginePage page;

    public GoogleSteps() {
    }

    @Given("user is on Google")
    public void navigateToSearchEnginePage() {
        SearchEnginePage.getDriver().get("https://www.google.com");
    }

    @When("search input exists")
    public void inputTextExists() {
        Assert.assertTrue(this.page.getInput() != null);
    }

    @Then("search for <searchCriteria>")
    public void searchForCriteria(@Named("searchCriteria") String searchCriteria) {
        this.page.searchForText(searchCriteria);
    }
}
