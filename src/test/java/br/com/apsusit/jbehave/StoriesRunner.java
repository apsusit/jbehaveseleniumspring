package br.com.apsusit.jbehave;

import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.UsingSteps;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.junit.spring.SpringAnnotatedEmbedderRunner;
import org.junit.runner.RunWith;

import java.util.List;

/**
 * Created by wesley on 5/29/16.
 */
@RunWith(SpringAnnotatedEmbedderRunner.class)
@Configure
@UsingEmbedder(embedder = Embedder.class, generateViewAfterStories = true, ignoreFailureInStories = true, ignoreFailureInView = false, stepsFactory = true)
@UsingSpring(resources = "classpath:config.xml")
@UsingSteps
public class StoriesRunner extends JUnitStories {

    protected List<String> storyPaths() {
        List<String> storyPaths = new StoryFinder().findPaths(
                CodeLocations.codeLocationFromClass(this.getClass()),
                "**/*.story", "");
        return storyPaths;
    }
}
