Scenario: User searches on Google
Given user is on Google
When search input exists
Then search for <searchCriteria>

Examples:
|searchCriteria|
|Wipro Technologies|
|Londrina|
|Brazil|
|JBehave|

